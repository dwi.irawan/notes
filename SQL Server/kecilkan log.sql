-- Truncate the log by changing the database recovery model to SIMPLE.
ALTER DATABASE [db_name]
SET RECOVERY SIMPLE;
GO
-- Shrink the truncated log file to 1 MB.
-- u/ melihat nama log db, klik kanan nama db > properties > file > logical name
DBCC SHRINKFILE (Std_Log, 1);
GO
-- Reset the database recovery model.
ALTER DATABASE [db_name]
SET RECOVERY FULL;
GO