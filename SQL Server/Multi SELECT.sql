SELECT CompanyCode, AgentId
    FROM UniqueAgentIdToUniqueAgentId un
    WHERE un.UniqueAgentId = 
    (
        SELECT UniqueAgentId 
         FROM AgentProductTraining  where UniqueAgentId IN
           (
            SELECT q.LastChangeDate, a.UniqueAgentId 
            FROM QueueUpdates q, AgentProductTraining a 
            WHERE a.LastChangeDate >= q.LastChangeDate
            )
    )

--SUMBER: https://stackoverflow.com/questions/11868675/multi-select-sql-statement


--FJB dengan Cost Awal
            SELECT DISTINCT m.UCode_FJB  
                ,m.Tgl_FJB
                ,m.No_FJB
                --,c.Nama_Cust
                ,m.Ucode_Brg -- Code Motor
                ,v.Kode_Div
                ,d.UCode_Brg As UCode_Spare -- Code Sparepart
                ,b.Nama_Brg -- Sparepart
                --,d.Qty
                --,m.No_Polisi
                --,m.Jns_Service
                --,k.Nama_Kry
                ,pb.Hrg_Unit_Riil_MU As Cost
                ,d.Hrg_Unit_Riil_MU As Harga
                ,m.Ket

            FROM tb_mt_FJB m
	            RIGHT JOIN tb_dt_FJB_Brg d on d.UCode_FJB=m.UCode_FJB
                JOIN tb_dt_PB_Brg pb on pb.UCode_Brg=d.UCode_Brg
	            JOIN tb_m_Cust c on c.UCode_Cust=m.Ucode_Cust
	            JOIN tb_m_Div v on v.UCode_Div=m.Ucode_Div
	            JOIN tb_m_Kry k on k.UCode_Kry=m.Ucode_kry
	            JOIN tb_m_Brg b on b.UCode_Brg=d.UCode_Brg
            WHERE Tgl_FJB >= '2019-09-24 00:00:00' 
            ORDER BY m.UCode_FJB DESC
            -- AND UCode_Spare IN ()